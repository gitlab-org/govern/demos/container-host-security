FROM golang:1.14-stretch AS builder

ARG APPLICATION_PASSWORD

RUN mkdir -p /app
WORKDIR /app

COPY go.mod /app
COPY go.sum /app
RUN go mod download

COPY . /app
RUN go build -ldflags "-X main.applicationPassword=$APPLICATION_PASSWORD" -o /app/main

FROM ubuntu:bionic

RUN apt update && apt install -y iputils-ping

RUN mkdir -p /app
WORKDIR /app
COPY --from=builder /app/main /app/main
COPY --from=builder /app/templates/ /app/templates/

EXPOSE 5000

ENTRYPOINT ["./main"]
